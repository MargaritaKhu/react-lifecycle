import logo from './logo.svg';
import './App.css';
import React from 'react';

const array = ['R', 'E', 'A', 'C', 'T'];
const arrayList = array.map((word) => <li>{word}</li>);

class App extends React.Component {
  constructor() {
    super();
    this.state = { date: new Date() };
  }
  componentDidMount() {
    this.timerID = setInterval(() => this.tick(), 1000);
    console.log('Монтирование компонента');
  }
  componentDidUpdate() {
    console.log('Обновление компонента');
  }
  componentWillUnmount() {
    clearInterval(this.timerID);
    console.log('Размонтирование компонента');
  }
  tick() {
    this.setState({
      date: new Date(),
    });
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>New text</p>
          <a
            className="App-link"
            href="https://google.com"
            target="_blank"
            rel="noopener noreferrer"
          >
            New link
          </a>

          <div className="App-data-types">
            {'String, number, bool & null:'}
            <br />
            {7}
            {true}
            {null}
          </div>

          <ul className="App-array-list">{arrayList}</ul>

          <div className="App-time-checking">
            Time to check lifecycle in console using this clocks <br />
            {this.state.date.toLocaleTimeString()}
          </div>
        </header>
      </div>
    );
  }
}

export default App;
