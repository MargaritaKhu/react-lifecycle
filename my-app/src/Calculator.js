/* eslint-disable no-eval */
import React from 'react';
import './Calculator.css';
import store from './store.js';

class Calculator extends React.Component {
  constructor() {
    super();
    this.state = {
      out: '0',
    };
    this.refOutput = React.createRef();
  }

  tapeNumber(value) {
    let currentValue = value;
    let output = this.refOutput.current;

    this.setState({
      out: currentValue,
    });

    if (output.value === '0') {
      output.value = '';
    }

    output.value += currentValue;
  }

  tapeOperation(value) {
    let output = this.refOutput.current;

    if (value === 'reset') {
      output.value = '0';
    } else if (value === '=') {
      try {
        output.value = eval(output.value);
      } catch {
        output.value = 'Недопустимое значение';
        setTimeout(() => {
          output.value = '0';
        }, 1500);
      }
    }
  }

  render() {
    return (
      <div className="Calculator">
        <header className="Calculator-header">
          <div className="container">
            <div className="output">
              <input
                ref={this.refOutput}
                type="text"
                defaultValue={this.state.out}
              />
            </div>
            <div className="buttons">
              {store.buttons.map((item, index) => (
                <button
                  key={index}
                  onClick={() => {
                    this.tapeNumber(item.value);
                  }}
                >
                  {item.value}
                </button>
              ))}
              {store.operations.map((item, index) => (
                <button
                  key={index}
                  onClick={() => {
                    this.tapeOperation(item.value);
                  }}
                >
                  {item.value}
                </button>
              ))}
            </div>
          </div>
        </header>
      </div>
    );
  }
}

export default Calculator;
